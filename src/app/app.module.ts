import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// routing
import { routing } from './app.routing';

// modules
import { MaterialModule } from './site/modules/material.module';
import { DialogsModule } from './site/modules/dialogs.module';

// services
import { AvesService } from './site/services/aves.service';
import { DialogsService } from './site/services/dialogs.service';

// components
import { AppComponent } from './app.component';
import { AvesListComponent } from './site/components/aves/aves-list/aves-list.component';
import { HomeComponent } from './site/components/home/home.component';
import { NotFoundComponent } from './site/components/not-found/not-found.component';
import { NavComponent } from './site/components/nav/nav.component';
import { AvesCreateComponent } from './site/components/aves/aves-create/aves-create.component';


@NgModule({
  declarations: [
    AppComponent,
    AvesListComponent,
    HomeComponent,
    NotFoundComponent,
    NavComponent,
    AvesCreateComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    DialogsModule,
    BrowserAnimationsModule,
    routing,
    ReactiveFormsModule
  ],
  providers: [
    AvesService,
    DialogsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
