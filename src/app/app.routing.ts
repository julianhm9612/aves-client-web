import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components

import { AvesListComponent } from './site/components/aves/aves-list/aves-list.component';
import { HomeComponent } from './site/components/home/home.component';
import { NotFoundComponent } from './site/components/not-found/not-found.component';
import { AvesCreateComponent } from './site/components/aves/aves-create/aves-create.component';

const appRoutes: Routes = [
    { path: 'home', component: HomeComponent },
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'aves',
        children: [
            { path: '', redirectTo: 'listado', pathMatch: 'full' },
            { path: 'listado', component: AvesListComponent },
            { path: 'editar/:id', component: AvesCreateComponent }
        ]
    },
    { path: '**', component: NotFoundComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes); // rutas principales