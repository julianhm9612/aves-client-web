import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { CollectionViewer } from '@angular/cdk/collections/typings';
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import { of as observableOf } from 'rxjs/observable/of';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { merge } from 'rxjs/observable/merge';

// services
import { AvesService } from '../../../services/aves.service';
import { DialogsService } from '../../../services/dialogs.service';

// models
import { Ave } from '../../../models/ave';

// material
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-aves-list',
  templateUrl: './aves-list.component.html',
  styleUrls: ['./aves-list.component.css']
})
export class AvesListComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public result: any;
  public dataSource = new AvesDataSource(this._avesService);
  public displayedColumns = ['cdave', 'dsnombreComun', 'dsnombreCientifico', 'edit', 'delete'];

  // resultsLength = 0;
  // isLoadingResults = false;
  // isRateLimitReached = false;

  constructor(
    private _avesService: AvesService,
    private dialogsService: DialogsService
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    // this.dataSource.paginator = this.paginator;
  //   this.dataSource = new AvesDataSource(this._avesService, this.paginator);
  //   // If the user changes the sort order, reset back to the first page.
  //   this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

  //   merge(this.sort.sortChange, this.paginator.page)
  //     .pipe(
  //       startWith({}),
  //       switchMap(() => {
  //         this.isLoadingResults = true;
  //         return this.dataSource!.getRepoIssues(
  //           this.sort.active, this.sort.direction, this.paginator.pageIndex);
  //       }),
  //       map(data => {
  //         // Flip flag to show that loading has finished.
  //         this.isLoadingResults = false;
  //         this.isRateLimitReached = false;
  //         //this.resultsLength = data.total_count;

  //         return data;
  //       }),
  //       catchError(() => {
  //         this.isLoadingResults = false;
  //         // Catch if the GitHub API has reached its rate limit. Return empty data.
  //         this.isRateLimitReached = true;
  //         return observableOf([]);
  //       })
  //     ).subscribe(data => this.dataSource.data = data);
  }

  openDialog(ave: string) {
    this.dialogsService
      .confirm('Eliminar ave ' + ave, 'Seguro que quieres hacer esto?')
      .subscribe(res => this.showMessage(res, ave, 'Se ha eliminado la ave'));
  }

  showMessage(response: boolean, id: any, message: string) {
    if (response !== undefined && response === true ) {
      this._avesService.deleteAve(id)
        .subscribe(
          res => this.dataSource = new AvesDataSource(this._avesService)
        );
    }
  }
}

export class AvesDataSource extends DataSource<any> {
  constructor(private _avesService: AvesService) {
    super();
  }
  connect(): Observable<Ave[]> {
    return this._avesService.getAllAves();
  }
  disconnect() { }
}