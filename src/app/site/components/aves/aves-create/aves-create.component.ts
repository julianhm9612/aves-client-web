import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

// services
import { AvesService } from '../../../services/aves.service';

// models
import { Ave } from '../../../models/ave';

@Component({
  selector: 'app-aves-create',
  templateUrl: './aves-create.component.html',
  styleUrls: ['./aves-create.component.css']
})
export class AvesCreateComponent implements OnInit {
  public ave: any;
  public cdAve: string;
  public aveForm: FormGroup;
  public zonas: any;
  public paises: any;
  public paisesForSelect: any;
  public zona: any;

  constructor(
    private route: ActivatedRoute,
    private _avesService: AvesService
  ) {
    this.cdAve = this.route.snapshot.params.id;
    this.zonas = [];
    this.paises = [];
    this.ave = {
      cdave: this.cdAve,
      dsnombreCientifico: '',
      dsnombreComun: ''
    };
  }

  ngOnInit() {
    this.getAve(this.cdAve);
    this.getZonas();
    this.getPaises();
    this.createForm();
  }

  private createForm() {
    this.aveForm = new FormGroup ({
      cdAve: new FormControl({ value: this.cdAve, disabled: true }, {
        validators: [
          Validators.required,
          Validators.maxLength(3)
        ],
      }),
      nombreComun: new FormControl('', {
        validators: [
          Validators.required,
          Validators.maxLength(100)
        ],
      }),
      nombreCientifico: new FormControl('', {
        validators: [
          Validators.required,
          Validators.maxLength(100)
        ],
      }),
      zona: new FormControl('', {
        validators: [
          Validators.required
        ],
      }),
      pais: new FormControl('', {
        validators: [
          Validators.required
        ],
      })
    });
  }

  getAve(ave: any) {
    this._avesService.getAve(ave)
      .subscribe(
        res => this.assingValue(res)
      );
  }

  getZonas() {
    this._avesService.getAllZonas()
    .subscribe(
      res => this.zonas = res
    );
  }

  getPaises() {
    this._avesService.getAllPaises()
    .subscribe(
      res => this.paises = res
    );
  }

  assingValue(res) {
    this.ave = res;
  }

  onSubmit(form: FormGroup) {
    console.log('Valid?', form.valid); // true or false
    console.log('Email', form.value.nombreComun);
    console.log('Message', form.value.nombreCientifico);
  }

  onChangeZona() {
    const that = this;
    this.paisesForSelect = this.paises.filter(function(x) {
      return x.cdzona.cdzona === that.zona;
    });
  }
}
