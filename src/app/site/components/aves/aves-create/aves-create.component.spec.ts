import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvesCreateComponent } from './aves-create.component';

describe('AvesCreateComponent', () => {
  let component: AvesCreateComponent;
  let fixture: ComponentFixture<AvesCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvesCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvesCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
