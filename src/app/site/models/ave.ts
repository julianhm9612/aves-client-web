export interface Ave {
    cdave: string;
    dsnombreComun: string;
    dsnombreCientifico: string;
    tontPaisesCollection: Array<any>;
}
