import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';

// services
import { GLOBAL } from './global';

// models
import { Ave } from '../models/ave';
import { Zona } from '../models/zona';
import { Pais } from '../models/pais';

// material
import { MatSnackBar } from '@angular/material';

@Injectable()
export class AvesService {
  public url: string;

  constructor(
    private _http: HttpClient,
    private snackBar: MatSnackBar
  ) {
    this.url = GLOBAL.url;
  }

  private log(message: string) {
    this.snackBar.open(message, 'Entendido', {
      duration: 3000,
    });
  }

  getAve(ave: Ave | string): Observable<Ave> {
    const id = typeof ave === 'string' ? ave : ave.cdave;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this._http.get<Ave>(this.url + '/tontaves/' + id, {headers: headers});
  }

  getAllAves(): Observable<Ave[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this._http.get<Ave[]>(this.url + '/tontaves', {headers: headers});
  }

  getAllZonas(): Observable<Zona[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this._http.get<Zona[]>(this.url + '/tontzonas', {headers: headers});
  }

  getAllPaises(): Observable<Pais[]> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this._http.get<Pais[]>(this.url + '/tontpaises', {headers: headers});
  }

  deleteAve(ave: Ave | string): Observable<Ave> {
    const id = typeof ave === 'string' ? ave : ave.cdave;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this._http.delete<Ave>(this.url + '/tontaves/' + id, {headers: headers})
      .pipe(
        tap(_ => this.log(`Se ha eliminado el ave id ${id}`))
      );
  }

}
